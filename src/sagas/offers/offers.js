// @flow
import type { Saga } from "redux-saga";
import { put, takeEvery, call } from "redux-saga/effects";
import { apis } from "../api/api";
import { FETCH_OFFERS } from "../../constants/actions";
import { PENDING, FULFILLED, REJECTED } from "../../utils/promiseUtils";
import type { Action } from "../../types/common";

export function* fetchoffers(action: Action): Saga<void> {
  yield put({ type: PENDING(action.type) });

  try {
    const data = yield call(apis.offers.get, {
      searchTerm: "Mallorca,%20Spanien"
    });
    yield put({ type: FULFILLED(action.type), data });
  } catch (error) {
    yield put({ type: REJECTED(action.type), error });
  }
}

export const offersSaga = [takeEvery(FETCH_OFFERS, fetchoffers)];
