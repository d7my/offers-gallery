import { runSaga } from "redux-saga";
import { cloneableGenerator } from "@redux-saga/testing-utils";
import { fetchoffers } from "./offers";

jest.mock("../api/api", () => ({
  apis: {
    offers: {
      get: jest.fn(() => ({}))
    }
  }
}));

describe("fetchoffers", () => {
  describe("when called with an action", () => {
    it("should call dispatch twice", async () => {
      const dispatchFn = jest.fn();
      await runSaga(
        {
          dispatch: dispatchFn
        },
        fetchoffers,
        { type: "MY_ACTION" }
      ).done;

      expect(dispatchFn).toHaveBeenCalledTimes(2);
      expect(dispatchFn).toHaveBeenCalledWith({ type: "PENDING_MY_ACTION" });
      expect(dispatchFn).toHaveBeenCalledWith({
        type: "FULFILLED_MY_ACTION",
        data: {}
      });
    });
  });
});
