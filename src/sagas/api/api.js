// @flow
const BASE_URL = "https://api.holidu.com/rest/v6/";

type GetParams = {
  [string]: string
};

const API = {
  get: async (path: string, params: ?GetParams = {}) => {
    const searchParams = new URLSearchParams();
    const endpoint = `${BASE_URL}${path}`;

    for (const key in params) {
      searchParams.append(key, params[key]);
    }
    const stringifiedParams = searchParams.toString();
    const url = `${endpoint}${
      stringifiedParams ? "?" : ""
    }${stringifiedParams}`;

    const response = await fetch(url);
    const data = await response.json();

    return data;
  }
};

const apis = {
  offers: {
    get: (params: ?GetParams) => API.get("search/offers", params)
  }
};

export { apis };
