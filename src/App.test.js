import React from "react";
import { render } from "@testing-library/react";
import App from "./App";
import MockProvider from "../tests/mocks/MockProvider";

const state = {
  offers: {
    list: [
      {
        id: "463925",
        photos: [],
        details: {
          name: "Offer title"
        },
        price: {
          total: 213
        },
        rating: {
          value: 90,
          count: 16
        }
      }
    ]
  }
};

describe("<App />", () => {
  it("should call dispatch once", () => {
    const getState = jest.fn(() => state);
    const dispatch = jest.fn();
    const { container } = render(
      <MockProvider getStateFn={getState} dispatchFn={dispatch}>
        <App />
      </MockProvider>
    );
    expect(dispatch).toHaveBeenCalledTimes(1);
  });

  it("should should render loader when isOffersLoading is true", () => {
    const getState = jest.fn(() => ({ offers: { isLoading: true } }));
    const { queryByTestId } = render(
      <MockProvider getStateFn={getState}>
        <App />
      </MockProvider>
    );
    const loader = queryByTestId("loader");
    expect(loader).toBeInTheDocument();
  });
});
