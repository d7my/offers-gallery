// @flow
import { useState } from "react";
import { useInView as useIntersectionObserver } from "react-intersection-observer";

export const useInView = (options: any) => {
  const [ref, inView] = useIntersectionObserver(options);
  const [isLoadedBefore, setIsloadedBefore] = useState(false);

  if (inView && !isLoadedBefore) {
    setIsloadedBefore(true);
  }

  return [ref, inView || isLoadedBefore];
};
