// @flow
import "rsuite/dist/styles/rsuite-default.css";
import "./index.css";
import "intersection-observer";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import configureStore from "./store/configureStore";

const store = configureStore();

const renderApp = () => {
  const root = document.getElementById("root");

  if (root !== null) {
    ReactDOM.render(
      <Provider store={store}>
        <App />
      </Provider>,
      root
    );
  }
};

renderApp();
