// @flow
import { applyMiddleware, createStore, combineReducers } from "redux";
import { all } from "redux-saga/effects";
import createSagaMiddleware from "redux-saga";
import offers from "../reducers/offers";
import { offersSaga } from "../sagas/offers/offers";

const rootReducer = combineReducers({
  offers
});

const rootSaga = function* rootSaga() {
  yield all([...offersSaga]);
};

export default function configureStore(preloadedState: Object) {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];
  const store = createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(...middlewares)
  );
  sagaMiddleware.run(rootSaga);

  return store;
}
