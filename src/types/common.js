export type Action = {
  type: string
};

export type Photo = {
  t: string,
  m: string,
  l: string,
  hr: string,
  overlay: boolean
};

export type Offer = {};

export type RatingType = {
  value: number,
  count: number
};
