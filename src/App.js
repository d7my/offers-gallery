// @flow
import "./App.css";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import Card from "./components/Card/Card";
import Navbar from "./components/Navbar/Navbar";
import { FETCH_OFFERS } from "./constants/actions";
import type { Offers } from "./types/common";
import { Loader } from "rsuite";
import { formatPrice } from "./utils/numberUtils";
import { trim } from "./utils/stringUtils";

const constructDescription = details =>
  `${details.guestsCount} guests, ${details.bedroomsCount} beds`;
const priceFormatter = formatPrice();
const trimTitle = trim();

type OffersCardsProps = {
  offers: Offers[]
};
const OffersCards = ({ offers }: OffersCardsProps) =>
  offers.map(offer => (
    <Card
      key={offer.id}
      title={trimTitle(offer.details.name)}
      photos={offer.photos.slice(0, 13)}
      description={constructDescription(offer.details)}
      price={priceFormatter(offer.price.total)}
      rating={offer.rating}
    />
  ));

type AppProps = {
  fetchOffers: () => void,
  offers: Offers[],
  isOffersLoading: boolean
};

export const App = ({ fetchOffers, offers, isOffersLoading }: AppProps) => {
  useEffect(() => {
    fetchOffers();
  }, [fetchOffers]);

  return (
    <div className="App">
      <Navbar />
      {isOffersLoading ? (
        <Loader speed="slow" size="lg" data-testid="loader" />
      ) : (
        <OffersCards offers={offers} />
      )}
    </div>
  );
};

const mapStateToProps = state => ({
  isOffersLoading: state.offers.isLoading,
  offers: state.offers.list
});
const mapDispatchToProps = dispatch => ({
  fetchOffers: () => {
    dispatch({ type: FETCH_OFFERS });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
