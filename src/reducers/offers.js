// @flow
import * as actions from "../constants/actions";
import type { Action, Offer } from "../types/common";
import { PENDING, REJECTED, FULFILLED } from "../utils/promiseUtils";

const initialState = {
  hasError: false,
  isLoading: false,
  list: []
};

type ApartmentsState = {
  isLoading: boolean,
  list: Offer[]
};

export default (
  state: ApartmentsState = initialState,
  action: Action
): ApartmentsState => {
  switch (action.type) {
    case PENDING(actions.FETCH_OFFERS): {
      return {
        ...state,
        isLoading: true
      };
    }
    case FULFILLED(actions.FETCH_OFFERS): {
      return {
        ...state,
        isLoading: false,
        list: action.data.offers
      };
    }
    case REJECTED(actions.FETCH_OFFERS): {
      return {
        ...state,
        isLoading: false,
        hasError: true
      };
    }
    default:
      return state;
  }
};
