import offers from "./offers";
import { PENDING, FULFILLED, REJECTED } from "../utils/promiseUtils";
import { FETCH_OFFERS } from "../constants/actions";

describe("offers reducers", () => {
  describe("when passing undefined as a state", () => {
    it("should return initiale state", () => {
      const expected = {
        hasError: false,
        isLoading: false,
        list: []
      };

      expect(offers(undefined, {})).toEqual(expected);
    });
  });

  describe("when passing PENDING_FETCH_OFFERS action", () => {
    it("should mark isLoading as true", () => {
      const state = offers(undefined, { type: PENDING(FETCH_OFFERS) });
      expect(state.isLoading).toEqual(true);
    });
  });

  describe("when passing FULFILLED_FETCH_OFFERS action", () => {
    it("should mark isLoading as false", () => {
      const action = {
        type: FULFILLED(FETCH_OFFERS),
        data: { offers: [{}, {}] }
      };
      const state = offers(undefined, action);
      expect(state.isLoading).toEqual(false);
    });

    it("should assign offers to list", () => {
      const action = {
        type: FULFILLED(FETCH_OFFERS),
        data: { offers: [{}, {}] }
      };

      const state = offers(undefined, action);
      expect(state.list).toEqual(action.data.offers);
    });
  });

  describe("when passing REJECTED_FETCH_OFFERS action", () => {
    it("should mark isLoading as false", () => {
      const action = {
        type: REJECTED(FETCH_OFFERS)
      };

      const state = offers(undefined, action);
      expect(state.hasError).toEqual(true);
    });
  });
});
