import { isString, trim } from "./stringUtils";
import { DEFAULT_OFFER_TITLE_LENGTH } from "../constants/base";

describe("isString", () => {
  describe("when passing string primitive", () => {
    it("should return true", () => {
      expect(isString("anthing")).toEqual(true);
    });
  });

  describe("when passing string object", () => {
    it("should return true", () => {
      const input = new String("anthing");
      expect(isString(input)).toEqual(true);
    });
  });

  describe("when passing undefined", () => {
    it("should return false", () => {
      expect(isString()).toEqual(false);
    });
  });

  describe("when passing null", () => {
    it("should return false", () => {
      expect(isString()).toEqual(false);
    });
  });

  describe("when passing an array", () => {
    it("should return false", () => {
      expect(isString([])).toEqual(false);
    });
  });
});

describe("trim", () => {
  const text =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

  describe("when not passing count", () => {
    it("should return a trimmer method and trim with default count", () => {
      const trimmer = trim();
      const trimmedText = trimmer(text);

      expect(typeof trimmer).toEqual("function");
      expect(trimmedText.length).toEqual(DEFAULT_OFFER_TITLE_LENGTH);
    });
  });

  describe("when not passing count", () => {
    it("should return a trimmer method", () => {
      const count = 33;
      const trimmer = trim(count);
      const trimmedText = trimmer(text);

      expect(typeof trimmer).toEqual("function");
      expect(trimmedText.length).toEqual(count);
    });
  });
});
