// @flow
import { DEFAULT_OFFER_TITLE_LENGTH } from "../constants/base";

export const isString = (text: any): boolean =>
  text ? typeof text.valueOf() === "string" : false;

export const trim = (count: number = DEFAULT_OFFER_TITLE_LENGTH): function => (
  text: string
): string =>
  isString(text) && text.length
    ? text
        .split("")
        .splice(0, count)
        .join("")
    : "";
