import { formatPrice } from "./numberUtils";

describe("formatPrice", () => {
  describe("when passing undefined", () => {
    it("should return a formatter method", () => {
      const actual = formatPrice();

      expect(typeof actual).toEqual("function");
    });

    it("should format price with default options", () => {
      const priceFormatter = formatPrice();
      const actual = priceFormatter(1100);
      const expected = "€1,100";

      expect(actual).toEqual(expected);
    });
  });

  describe("when passing USD currency", () => {
    it("should should format price in USD", () => {
      const priceFormatter = formatPrice({ currency: "USD" });
      const actual = priceFormatter(1100);
      const expected = "$1,100";

      expect(actual).toEqual(expected);
    });
  });
});
