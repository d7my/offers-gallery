// @flow
import { DEFAULT_LOCALE } from "../constants/base";

export const formatPrice = (
  options: Object = {},
  locale: string = DEFAULT_LOCALE
) => {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "EUR",
    minimumFractionDigits: 0,
    ...options
  });

  return (price: number) => formatter.format(price);
};
