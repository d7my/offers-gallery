// @flow

export const FULFILLED = (asycnAction: string): string =>
  `FULFILLED_${asycnAction}`;
export const PENDING = (asycnAction: string): string =>
  `PENDING_${asycnAction}`;
export const REJECTED = (asycnAction: string): string =>
  `REJECTED_${asycnAction}`;
