import React from "react";
import { render } from "@testing-library/react";
import { mockAllIsIntersecting } from "react-intersection-observer/test-utils";
import Card from "./Card";

const baseProps = {
  title: "Card Title",
  description: "Card description",
  photos: [
    {
      t: "http://example.com/images/2649350234/t.jpg"
    },
    {
      t: "http://example.com/images/29367845834/t.jpg"
    }
  ]
};

describe("<Card />", () => {
  describe("when passing title, description and photos", () => {
    it("should render card container", () => {
      const { queryByTestId } = render(<Card {...baseProps} />);
      const cardContainer = queryByTestId("card-container");

      expect(cardContainer).toBeInTheDocument();
    });

    it("should render card title", () => {
      const { queryByTestId } = render(<Card {...baseProps} />);
      const title = queryByTestId("card-title");

      expect(title.innerHTML).toEqual(baseProps.title);
    });

    it("should render card description", () => {
      const { queryByTestId } = render(<Card {...baseProps} />);
      const title = queryByTestId("card-description");

      expect(title.innerHTML).toEqual(baseProps.description);
    });

    it("should render all card photos", () => {
      const { queryAllByTestId, container } = render(<Card {...baseProps} />);
      mockAllIsIntersecting(true);
      const cardPhotos = queryAllByTestId("card-photo");

      expect(cardPhotos.length).toEqual(baseProps.photos.length * 2);
    });

    it("shouldn't render card price", () => {
      const { queryByTestId } = render(<Card {...baseProps} />);
      const price = queryByTestId("card-price");

      expect(price).not.toBeInTheDocument();
    });
  });

  describe("when passing title, description, photos and price", () => {
    it("should render price", () => {
      const props = {
        ...baseProps,
        price: 111
      };
      const { queryByTestId } = render(<Card {...props} />);
      const price = queryByTestId("card-price");

      expect(price.innerHTML).toEqual("111");
    });
  });

  describe("when passing title, description, photos, price, rating", () => {
    it("should render rating", () => {
      const props = {
        ...baseProps,
        price: 111,
        rating: {
          value: 90
        }
      };
      const { queryAllByTestId } = render(<Card {...props} />);
      const ratingStars = queryAllByTestId(/rating-star/);

      expect(ratingStars.length).toEqual(5);
    });
  });
});
