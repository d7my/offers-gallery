// @flow
import "./Card.css";
import * as React from "react";
import { Carousel, Divider, Button } from "rsuite";
import type { Photo, RatingType } from "../../types/common";
import Rating from "../Rating/Rating";
import { useInView } from "../../hooks/useInView";

type CardProps = {
  title: string,
  description: string,
  price?: string,
  rating: RatingType,
  photos: Photo[]
};
const Card = ({ photos, title, description, price, rating }: CardProps) => {
  const [ref, inView] = useInView();

  return (
    <div ref={ref} className="card" data-testid="card-container">
      <Carousel className="card__slider" placement="bottom" shape="bar">
        {photos.map((photo, index) =>
          inView ? (
            <img
              data-testid="card-photo"
              key={photo.t}
              src={photo.t}
              alt={`${title} ${index + 1}`}
            />
          ) : (
            <div key={photo.t} />
          )
        )}
      </Carousel>

      <div className="card__content">
        <div className="card__content__header">
          <h4 className="card__content__title" data-testid="card-title">
            {title}
          </h4>
          {price && (
            <div className="card__content__price" data-testid="card-price">
              {price}
            </div>
          )}
        </div>

        <div className="card__content__body">
          <p
            className="card__content__body__description"
            data-testid="card-description"
          >
            {description}
          </p>
          {rating && (
            <div className="card__content__body__rating">
              <Rating percentage={rating.value} />
              <span className="card__content__body__rating__count">
                {rating.count}
              </span>
            </div>
          )}
        </div>

        <Divider className="card__content__divider" />

        <div className="card__content__footer">
          <Button color="red">View Offer</Button>
        </div>
      </div>
    </div>
  );
};

export default Card;
