// @flow
import React from "react";
import { Navbar as RsuiteNavbar, Nav, Icon } from "rsuite";
import "./Navbar.css";

const Navbar = () => {
  return (
    <RsuiteNavbar
      className="navbar"
      appearance="inverse"
      data-testid="navbar-container"
    >
      <RsuiteNavbar.Body>
        <Nav>
          <Nav.Item eventKey="1" icon={<Icon icon="home" />}>
            Home
          </Nav.Item>
          <Nav.Item eventKey="2" icon={<Icon icon="search" />}>
            Discover
          </Nav.Item>
        </Nav>
        <Nav pullRight>
          <Nav.Item icon={<Icon icon="user" />}>Sign in</Nav.Item>
        </Nav>
      </RsuiteNavbar.Body>
    </RsuiteNavbar>
  );
};

export default Navbar;
