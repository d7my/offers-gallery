import React from "react";
import { render } from "@testing-library/react";
import Navbar from "./Navbar";

describe("<Navbar />", () => {
  it("should render navbar", () => {
    const { queryAllByTestId } = render(<Navbar />);
    const navbarContainer = queryAllByTestId("navbar-container");

    expect(navbarContainer.length).toEqual(1);
  });
});
