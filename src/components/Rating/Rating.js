// @flow
import * as React from "react";
import { Icon } from "rsuite";
import "./Rating.css";

type RatingProps = {
  percentage: number
};

const Rating = ({ percentage }: RatingProps) => {
  const ratingOutOfFive = parseInt((percentage / 100) * 5);
  const times: Array<number> = [1, 2, 3, 4, 5];

  return times.map<React.Node>((time: number) => {
    const isFilled = time <= ratingOutOfFive;

    return (
      <Icon
        data-testid={`rating-star-${isFilled ? "filled" : "empty"}`}
        className="rating"
        key={time}
        icon={isFilled ? "star" : "star-o"}
      />
    );
  });
};

export default Rating;
