import React from "react";
import { render } from "@testing-library/react";
import Rating from "./Rating";

describe("<Rating >", () => {
  describe("when percentage is 100%", () => {
    it("should render all 5 stars as filled", () => {
      const { queryAllByTestId } = render(<Rating percentage={100} />);
      const filledRatingStars = queryAllByTestId("rating-star-filled");
      const emptyRatingStars = queryAllByTestId("rating-star-empty");

      expect(filledRatingStars.length).toEqual(5);
      expect(emptyRatingStars.length).toEqual(0);
    });
  });

  describe("when percentage is 70%", () => {
    it("should render 3 filled stars and 2 empty ones", () => {
      const { queryAllByTestId } = render(<Rating percentage={70} />);
      const filledRatingStars = queryAllByTestId("rating-star-filled");
      const emptyRatingStars = queryAllByTestId("rating-star-empty");

      expect(filledRatingStars.length).toEqual(3);
      expect(emptyRatingStars.length).toEqual(2);
    });
  });
});
