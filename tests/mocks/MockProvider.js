import React from "react";
import { Provider } from "react-redux";

const MockProvider = ({ children, dispatchFn, getStateFn }) => {
  const store = {
    getState: getStateFn || jest.fn(() => ({})),
    dispatch: dispatchFn || jest.fn(),
    subscribe: jest.fn(),
    replaceReducer: jest.fn()
  };

  return (<Provider store={store}>{children}</Provider>);
};

export default MockProvider;
