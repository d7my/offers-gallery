#### Application overview
**Offers Gallery** app consists of multiple UI components like `Card`, `Rating` and one  main component  `App`.

#### Naming convention and file structure
- File name should represent the exported convention

```js
const Card = () => (...)
export default Card // should have (Card.js) as a file name
```

```js
const useInView = () => (...)
export { useInView }  // should have (useInView.js) as a file name
```

- Evey component should have its own directory contains its styles and tests

```
├── Card
│   ├── Card.js
│   ├── Card.test.js
│   └── Card.css
```

- Shared modules should consider its usage. e.g. `api/` directory shouldn't be at the top level as
it's used only inside `sagas/` directory.

#### Notes

- Images should be wrapped by `intersectionObserver` to avoid loading images outside `viewport` and you can use `useInView` hook.

```js
const [ref, inView] = useInView();

{inView ? (
  <img
    src='http://example/images/t.jpg'
  />
) : (
  <div /> // should be placeholder image
)}
```

- This project uses `Intl.NumberFormat` to format prices, so we will need to add polyfill if we want to support older browsers

***

#### Installation

- clone this repo `git clone git@bitbucket.org:d7my/offers-gallery.git`
- navigate to Offers Gallery App directory `cd offers-gallery`
- run `yarn` to install dependencies
- run `yarn start` to start development server
- will open a tab with `http://localhost:3000/`


#### Testing
execute the following command to run application tests.

```sh-session
yarn test
```

I used `@testing-library/react` to test components behaviours.


#### Linting
execute the following command to run prettier

```sh-session
yarn prettier
```

#### FlowJS
execute the following command to run flow

```sh
yarn flow
```

#### Additional Points:
- I used flowJS for static typing.
- I covered a lot of cases with testing but the coverage could be increased.
- I used `rsuite` as a UI library.
- I handled the `loading` state for the `<App />` component but didn't handle error case as it's almost the same
